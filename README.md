The unofficial higan and bsnes repository
=========================================

Once, this repository was the place
to track the development of the higan console emulator
and related projects.
This repository is no longer updated,
development has moved to official repos:

  - https://github.com/byuu/higan
    contains history of higan the multi-system emulator,
    previously the `old-master` branch of this repo
  - https://github.com/byuu/bsnes
    contains the history of bsnes the SNES-focused emulator,
    previously the `bsnes-v107` branch of this repo
    
This repo's `master` branch contains only this README.
The former contents of the `master` branch
(the history of higan)
is available as the `old-master` branch.

Official resources
------------------

  - [higan homepage](https://byuu.org/emulation/higan/)
  - [higan git repo](https://github.com/byuu/higan/)
  - [bsnes homepage](https://byuu.org/emulation/bsnes/)
  - [bsnes git repo](https://github.com/byuu/bsnes/)

Unofficial resources
--------------------

  - [Unofficial forum](https://helmet.kafuka.org/bboard/)
